<?php
namespace Brown298\DataTablesModels\Tests\Service;

use \Brown298\DataTablesModels\Service\ServerProcessService;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class ServerProcessServiceTest
 *
 * @package Brown298\DataTablesModels\Tests\Service
 * @author  John Brown <brown.john@gmail.com>
 */
class ServerProcessServiceTest extends AbstractTest
{

    /**
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    protected $request;

    /**
     * @var \Brown298\DataTablesModels\Model\RequestParameterBag
     */
    protected $requestParameters;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Brown298\DataTablesModels\Service\ServerProcessService
     */
    protected $service;

    /**
     * @var \Brown298\DataTablesModels\Service\Processor\ProcessorInterface
     */
    protected $processor;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $arrayCollection;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();

        $this->service           = new ServerProcessService();
        $this->request           = Phake::mock('\Psr\Http\Message\ServerRequestInterface');
        $this->requestParameters = Phake::mock('Brown298\DataTablesModels\Model\RequestParameterBag');
        $this->logger            = Phake::mock('Psr\Log\LoggerInterface');
        $this->processor         = Phake::mock('Brown298\DataTablesModels\Service\Processor\ProcessorInterface');
        $this->arrayCollection   = Phake::mock('Doctrine\Common\Collections\ArrayCollection');

        $this->service->setRequest($this->request);
    }

    /**
     * testCreate
     */
    public function testCreate()
    {
        $this->assertInstanceOf('\Brown298\DataTablesModels\Service\ServerProcessService', $this->service);
    }

    /**
     * testGetSetData
     */
    public function testGetSetData()
    {
        $data = array('test');
        $this->service->setData($data);
        $this->assertEquals($data, $this->service->getData());
    }

    /**
     * testGetDataNoProcessorReturnsNull
     */
    public function testGetDataNoProcessorReturnsNull()
    {
        $this->assertNull($this->service->getData());
    }

    /**
     * testGetSetRequest
     */
    public function testGetSetRequest()
    {
        $this->assertEquals($this->request, $this->service->getRequest());
        $this->assertInstanceOf('\Brown298\DataTablesModels\Model\RequestParameterBag', $this->service->getRequestParameters());
    }

    /**
     * testGetSetColumnsEmpty
     */
    public function testGetSetColumnsEmpty()
    {
        $this->service->setColumns(array());
        $this->assertEquals(array(), $this->service->getColumns());
    }

    /**
     * testGetSetColumnsValue
     */
    public function testGetSetColumnsValue()
    {
        $this->service->setColumns(array('test'));
        $this->assertEquals(array('test'), $this->service->getColumns());
    }

    /**
     * testAddColumn
     */
    public function testAddColumn()
    {
        $this->service->addColumn('test','123');
        $this->assertEquals(array('test'=>'123'), $this->service->getColumns());
    }


    /**
     * testGetResponseParameters
     */
    public function testGetResponseParameters()
    {
        $this->assertNull($this->service->getResponseParameters());

        $responseParameters = Phake::mock('Brown298\DataTablesModels\Model\ResponseParameterBag');
        $this->setProtectedValue($this->service, 'responseParameters', $responseParameters);

        $this->assertEquals($responseParameters, $this->service->getResponseParameters());
    }


    /**
     * testProcessWithDataEmpty
     */
    public function testProcessWithDataEmpty()
    {
        $data          = array();
        $dataFormatter = null;

        $this->service->setData($data);
        $result = $this->service->process($dataFormatter);

        $this->assertEquals(array(
            'sEcho'                => null,
            'iTotalRecords'        => 0,
            'iTotalDisplayRecords' => 0,
            'aaData'               => array(),
        ), $result);
    }

    /**
     * testProcessWithData
     */
    public function testProcessWithData()
    {
        $data          = array('test');
        $dataFormatter = null;
        $this->setProtectedValue($this->service, 'requestParameters', $this->requestParameters);
        Phake::when($this->requestParameters)->getDisplayLength()->thenReturn(10);

        $this->service->setData($data);
        $result = $this->service->process($dataFormatter);

        $this->assertEquals(array(
            'sEcho'                => null,
            'iTotalRecords'        => 1,
            'iTotalDisplayRecords' => 1,
            'aaData'               => array('test'),
        ), $result);
    }

    /**
     * testProcessCallsFormatter
     *
     * ensure the formatting function gets called
     */
    public function testProcessCallsFormatter()
    {
        $data          = array('test');
        $dataFormatter = function($data) {
            return array('123');
        };
        $this->setProtectedValue($this->service, 'requestParameters', $this->requestParameters);
        Phake::when($this->requestParameters)->getDisplayLength()->thenReturn(10);

        $this->service->setData($data);
        $result = $this->service->process($dataFormatter);

        $this->assertEquals(array(
            'sEcho'                => null,
            'iTotalRecords'        => 1,
            'iTotalDisplayRecords' => 1,
            'aaData'               => array('123'),
        ), $result);
    }

    /**
     * testDebug
     */
    public function testDebug()
    {
        $this->service->setLogger($this->logger);

        $this->service->debug('test');

        Phake::verify($this->logger)->debug('test');
    }

    /**
     * testProcessThrowsErrorWithoutProcessorDefined
     *
     * @expectedException Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testProcessThrowsErrorWithoutProcessorDefined()
    {
        $this->service->process();
    }

    /**
     * testGetSetProcessor
     *
     */
    public function testGetSetProcessor()
    {
        $this->service->setProcessor($this->processor);
        $this->assertEquals($this->processor, $this->service->getProcessor());
    }

}