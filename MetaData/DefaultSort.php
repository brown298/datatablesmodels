<?php
namespace Brown298\DataTablesModels\MetaData;

/**
 * Class DefaultSort
 *
 * @package Brown298\DataTablesModels\MetaData
 * @author  John Brown <brown.john@gmail.com>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class DefaultSort
{

}
