<?php

namespace Brown298\DataTablesModels\MetaData;

/**
 * Class Format
 *
 * @package Brown298\DataTablesModels\MetaData
 * @author  John Brown
 *
 * @Annotation
 */
class Format
{
    /**
     * @var array
     */
    public $dataFields;

    /**
     * @var string
     */
    public $template;
}
