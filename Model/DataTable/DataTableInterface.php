<?php
namespace Brown298\DataTablesModels\Model\DataTable;

use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DataTableInterface
 *
 * @package Brown298\DataTablesModels\Model\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
interface DataTableInterface
{
    /**
     * getColumns
     *
     * @return array
     */
    public function getColumns();

    /**
     * setColumns
     *
     * @param array|null $columns
     * @return null
     */
    public function setColumns(array $columns = null);

    /**
     * isAjaxRequest
     *
     * @param RequestInterface $request
     * @return mixed
     */
    public function isAjaxRequest(RequestInterface $request);

    /**
     * @param callable $dataFormatter
     * @return mixed
     */
    public function setDataFormatter(\Closure $dataFormatter = null);

    /**
     * @return mixed
     */
    public function getDataFormatter();

    /**
     * @param array $metaData
     * @return mixed
     */
    public function setMetaData(array $metaData = null);

    /**
     * @return mixed
     */
    public function getMetaData();

    /**
     * getData
     *
     * @param RequestInterface $request
     *
     * @return mixed
     */
    public function getData(RequestInterface $request);

    /**
     * getJsonResponse
     *
     * @param RequestInterface $request
     * @param callable|null $dataFormatter
     *
     * @return mixed
     */
    public function getJsonResponse(RequestInterface $request, \Closure $dataFormatter = null);


    /**
     * processRequest
     *
     * processes a request and returns the appropriate response
     *
     * @param RequestInterface $request
     * @param callable $dataFormatter
     * @return mixed false if not an ajax request
     */
    public function process(RequestInterface $request, \Closure $dataFormatter = null);

}
