<?php
namespace Brown298\DataTablesModels\Service;

use Brown298\DataTablesModels\Exceptions\ProcessorException;
use Brown298\DataTablesModels\Service\AbstractServerProcessor;
use Brown298\DataTablesModels\Service\Interfaces\ArrayProcessInterface;
use Brown298\DataTablesModels\Service\Interfaces\ServerProcessServiceInterface;
use Brown298\DataTablesModels\Service\Processor\ArrayProcessor;

/**
 * Class ServerProcessService
 *
 * handles the server side processing
 *
 * @package Brown298\DataTablesModels\Service
 * @author  John Brown <brown.john@gmail.com>
 */
class ServerProcessService extends AbstractServerProcessor implements ServerProcessServiceInterface, ArrayProcessInterface
{

    /**
     * @param array $data
     * @return mixed|void
     */
    public function setData(array $data)
    {
        $this->processor = new ArrayProcessor($this->requestParameters, $this->logger);
        $this->processor->setData($data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if ($this->processor === null || !($this->processor instanceof ArrayProcessor)) {
            return null;
        }

        return $this->processor->getData();
    }

}
