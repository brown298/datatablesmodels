<?php
namespace Brown298\DataTablesModels\Service;

use Brown298\DataTablesModels\Exceptions\ProcessorException;
use Brown298\DataTablesModels\Model\RequestParameterBag;
use Brown298\DataTablesModels\Model\ResponseParameterBag;
use Brown298\DataTablesModels\Service\Processor\ProcessorInterface;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface;


/**
 * Class AbstractServerProcessor
 *
 *
 * @package Brown298\DataTablesModels\Service
 * @author  John Brown <brown.john@gmail.com>
 */
abstract class AbstractServerProcessor
{
    /**
     * @var mixed
     *
     * \Brown298\DataTablesModels\Service\Processor\ArrayProcessor
     */
    protected $processor;

    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * @var \Brown298\DataTablesModels\Model\RequestParameterBag
     */
    protected $requestParameters;

    /**
     * @var \Brown298\DataTablesModels\Model\ResponseParameterBag
     */
    protected $responseParameters;

    /**
     * @var null|PSR\Log|Logger
     */
    protected $logger = null;

    /**
     * process
     *
     * gets the results
     *
     * @param null $dataFormatter
     * @param bool $getEntity
     *
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     * @return array
     */
    public function process($dataFormatter = null, $getEntity = false)
    {
        if (!($this->processor instanceof ProcessorInterface)) {
            Throw new ProcessorException("DataTables Processor not defined did you forget to set the data or add a query?");
        }

        $this->responseParameters = new ResponseParameterBag();
        $this->responseParameters->setRequest($this->requestParameters);

        $this->responseParameters = $this->processor->process($this->responseParameters, $getEntity);

        return $this->responseParameters->all($dataFormatter);
    }

    /**
     * @param ServerRequestInterface $request
     */
    public function setRequest(ServerRequestInterface $request)
    {
        $this->request = $request;
        $this->requestParameters = new RequestParameterBag();
        $this->requestParameters->fromRequest($request);
    }

    /**
     * getRequest
     *
     * @return ServerRequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * addColumn
     *
     * @param $name
     * @param $title
     */
    public function addColumn($name, $title)
    {
        $this->requestParameters->addColumn($name, $title);
    }

    /**
     * setColumns
     *
     * @param array $columns
     */
    public function setColumns(array $columns)
    {
        $this->requestParameters->setColumns($columns);
    }

    /**
     * getColumns
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->requestParameters->getColumns();
    }

    /**
     * @return \Brown298\DataTablesModels\Model\RequestParameterBag
     */
    public function getRequestParameters()
    {
        return $this->requestParameters;
    }

    /**
     * @return \Brown298\DataTablesModels\Model\ResponseParameterBag
     */
    public function getResponseParameters()
    {
        return $this->responseParameters;
    }

    /**
     * setLogger
     *
     * @param $logger
     */
    public function setLogger(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * debug
     *
     * @param $message
     */
    public function debug($message)
    {
        if ($this->logger instanceof LoggerInterface) {
            $this->logger->debug($message);
        }
    }

    /**
     * setProcessor
     *
     * @param ProcessorInterface $processor
     */
    public function setProcessor(ProcessorInterface $processor)
    {
        $this->processor = $processor;
    }

    /**
     * getProcessor
     *
     * @return mixed
     */
    public function getProcessor()
    {
        return $this->processor;
    }
}
