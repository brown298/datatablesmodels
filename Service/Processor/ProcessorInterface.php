<?php
namespace Brown298\DataTablesModels\Service\Processor;

use Brown298\DataTablesModels\Model\ResponseParameterBag;

/**
 * Class ProcessorInterface
 * @package Brown298\DataTablesModels\Service\Processor
 * @author  John Brown <brown.john@gmail.com>
 */
interface ProcessorInterface
{
    /**
     * tells the processor to run
     *
     * @param ResponseParameterBag $responseParameters
     * @param bool $getEntity
     *
     * @return mixed
     */
    public function process(ResponseParameterBag $responseParameters = null, $getEntity = false);
}
