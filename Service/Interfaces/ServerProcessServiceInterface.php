<?php
namespace Brown298\DataTablesModels\Service\Interfaces;

use Brown298\DataTablesModels\Service\Processor\ProcessorInterface;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface ServerProcessServiceInterface
 * @package Brown298\DataTablesModels\Service
 */
interface ServerProcessServiceInterface
{
    /**
     * @param null $dataFormatter
     * @param bool $getEntity
     * @return mixed
     */
    public function process($dataFormatter = null, $getEntity = false);

    /**
     * setProcessor
     *
     * @param ProcessorInterface $processor
     */
    public function setProcessor(ProcessorInterface $processor);

    /**
     * getProcessor
     *
     * @return mixed
     */
    public function getProcessor();

    /**
     * setLogger
     *
     * @param $logger
     */
    public function setLogger(LoggerInterface $logger = null);

    /**
     * @param ServerRequestInterface $request
     */
    public function setRequest(ServerRequestInterface $request);

    /**
     * getRequest
     *
     * @return ServerRequestInterface
     */
    public function getRequest();

    /**
     * @return \Brown298\DataTablesModels\Model\RequestParameterBag
     */
    public function getRequestParameters();

    /**
     * @return \Brown298\DataTablesModels\Model\ResponseParameterBag
     */
    public function getResponseParameters();
}
