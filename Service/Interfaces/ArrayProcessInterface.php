<?php
namespace Brown298\DataTablesModels\Service\Interfaces;

/**
 * Interface ArrayProcessInterface
 * @package Brown298\DataTablesModels\Service\Interfaces
 */
interface ArrayProcessInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function setData(array $data);

    /**
     * @return mixed
     */
    public function getData();
}
