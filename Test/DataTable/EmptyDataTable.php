<?php
namespace Brown298\DataTablesModels\Test\DataTable;

use Psr\Http\Message\RequestInterface;
use Brown298\DataTablesModels\Model\DataTable\AbstractDataTable as AbstractBaseDataTable;
use Brown298\DataTablesModels\Model\DataTable\DataTableInterface as BaseDataTableInterface;

/**
 * Class EmptyDataTable
 *
 * @package Brown298\DataTablesModels\Model
 * @author  John Brown <brown.john@gmail.com>
 */
class EmptyDataTable extends AbstractBaseDataTable implements BaseDataTableInterface
{
    /**
     * getData
     *
     * @param RequestInterface $request
     *
     * @return array|mixed
     */
    public function getData(RequestInterface $request)
    {
        return array();
    }
}
