<?php
namespace Brown298\DataTablesModels\Exceptions;

/**
 * Class ProcessorException
 * @package Brown298\DataTablesModels\Exceptions
 * @author  John Brown <brown.john@gmail.com>
 */
class ProcessorException extends \Exception
{

}
