<?php
namespace Brown298\DataTablesModels\Exceptions;

/**
 * Class InvalidArgumentException
 *
 * indicates an invalid argument was used
 *
 * @package Brown298\DataTablesModels\Exceptions
 */
class InvalidArgumentException extends \Exception
{

}
