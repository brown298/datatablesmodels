<?php
namespace Brown298\DataTablesModels\Exceptions;

/**
 * Class ResourceNotFoundException
 *
 * indicates a resource required by datatable bundle is not available
 *
 * @package Brown298\DataTablesModels\Exceptions
 */
class ResourceNotFoundException extends \Exception
{

}
